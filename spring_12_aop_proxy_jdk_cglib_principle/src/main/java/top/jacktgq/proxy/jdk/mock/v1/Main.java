package top.jacktgq.proxy.jdk.mock.v1;

import top.jacktgq.proxy.jdk.mock.v2.Foo;

/**
 * @Author CandyWall
 * @Date 2022/4/11--10:29
 * @Description
 */
public class Main {
    public static void main(String[] args) {
        Foo proxy = new $Proxy0();
        proxy.foo();
    }
}
